import traceback


def first_call():
    print('I`m first call')


def second_call():
    inner_call()
    print('I`m second call')


def inner_call():
    # traceback.print_stack()
    print('I"m from inner from second')


def main():
    print('Hello from main')
    first_call()
    second_call()


main()
