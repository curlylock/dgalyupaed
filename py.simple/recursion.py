import traceback


class Node:
    value = 0
    children = []

    def __init__(self, val, children):
        self.value = val
        self.children = children


binary_tree = Node(
    20,
    [Node(
        7,
        [
            Node(
                4,
                Node(2, null)
            ),
            Node(
                15,
                [Node(11, null), Node(18, null)]
            )
        ]
    ), Node(53, null)]
)


def my_custom_iteration(stuct, index=0):
    if len(stuct) - 1 == index:
        print(stuct[index])
        return

    print(stuct[index])

    my_custom_iteration(stuct, index + 1)


def factorial(n):
    if n == 1:
        return 1

    return n * factorial(n-1)


def fibonacci(n):
    if n <= 1:
        traceback.print_stack()
        print("\n\n")
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


# my_custom_iteration([1, 2, 3, 4, 5])

fibonacci(3)


