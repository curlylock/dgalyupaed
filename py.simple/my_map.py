def my_map(func, struct):
    new_struct = []

    for o in struct:
        new_struct.append(func(o))

    return new_struct


def add_one(num):
    return num + 1


# print(my_map(add_one, [1, 2, 3, 4, 5]))


def say_hello_to(name):
    print(f'Hello here Hello hereHello hereHello hereHello here {name}')
    print(f'Hello here {name} hereHello hereHello hereHello here')
    print(f'Hello here Hello hereHello hereHello hereHello here {name}')
    print(f'Hello here Hello hereHello hereHello hereHello here {name}')


# say_hello_to('Denis')
