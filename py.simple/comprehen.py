class RandChars:
    def __init__(self):
        pass

    def __iter__(self):
        self.count = 0
        return self

    def __next__(self):
        self.count += 1

        if self.count > 3:
            raise StopIteration
        if self.count == 1:
            return 'I`m the first'
        elif self.count == 2:
            return []
        elif self.count == 3:
            return 'I like trains'


class Product:
    def __init__(self, idx, name, price):
        self.id = idx
        self.name = name
        self.price = price


# f = RandChars()
# f.hello()
#
# x = [1, 2, 3, 4, 5]
# x = (1, 2, 3, 4, 5)
# x = {1, 2, 3, 4, 5}

cart = [
    Product(1, 'ball', 20),
    Product(2, 'milk', 10),
    Product(3, 'dog', 50),
    Product(1, 'dog', 50),
    Product(1, 'bread', 50),
]

cart_map = {
    product.name: product.price
    for product in cart
}

cart_set = {product.name for product in cart}

# TODO tuple comprehensions

print(cart_set)

# deliveries = {
#     'express': [15, 7, 2],
#     'common': [1, 2, 3]
# }
#
#
# def send_by_delivery_type(del_type, product_name):
#     print(f'Send success by {del_type} delivery for for product {product_name}')
#
#
# for product_id in deliveries['express']:
#     if product_id in cart_map:
#         send_by_delivery_type('express', cart_map[product_id].name)

# y = list(map(lambda g: g * 2, x))
# u = [p * 2 for p in x]


# o = list(RandChars())

# print(o)
